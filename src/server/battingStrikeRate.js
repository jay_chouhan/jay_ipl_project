const battingStrikeRate = (deliveriesData, matchesData, playerName) => {
  const seasonMatchID = matchesData
    .map(match => {
      return ([match.id] = match.season);
    })
    .reduce(Object.assign, {});

  return deliveriesData
    .filter(delivery => delivery.batsman === playerName)
    .reduce((accumulator, delivery) => {
      if (accumulator[delivery.batsman] === undefined) {
        accumulator[delivery.batsman] = {};
      }

      if (
        accumulator[delivery.batsman][seasonMatchID[delivery.match_id]] ===
        undefined
      ) {
        accumulator[delivery.batsman][seasonMatchID[delivery.match_id]] = {
          runs: 0,
          balls: 0
        };
      }

      accumulator[delivery.batsman][
        seasonMatchID[delivery.match_id]
      ].runs += parseInt(delivery.batsman_runs);

      accumulator[delivery.batsman][
        seasonMatchID[delivery.match_id]
      ].balls += 1;

      accumulator[delivery.batsman][
        seasonMatchID[delivery.match_id]
      ].strikeRate = (
        (accumulator[delivery.batsman][seasonMatchID[delivery.match_id]].runs *
          100) /
        accumulator[delivery.batsman][seasonMatchID[delivery.match_id]].balls
      ).toFixed(2);
      return accumulator;
    }, {});
};

module.exports = battingStrikeRate;
