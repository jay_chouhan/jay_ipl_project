const extraRunsPerTeam = (deliveriesData, matchesData, season) => {
  let seasonId = matchesData
    .filter(match => match.season === season.toString())
    .reduce((accumulator, match) => {
      accumulator[match.id] = match;
      return accumulator;
    }, {});

  return deliveriesData
    .filter(delivery => seasonId[delivery.match_id])
    .reduce((accumulator, delivery) => {
      if (accumulator[delivery.bowling_team] === undefined) {
        accumulator[delivery.bowling_team] = 0;
      }
      accumulator[delivery.bowling_team] += parseInt(delivery.extra_runs);
      return accumulator;
    }, {});
};

module.exports = extraRunsPerTeam;
