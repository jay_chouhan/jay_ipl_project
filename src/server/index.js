const csv = require("csvtojson");
const fs = require("fs");

const matchesPerYear = require("./matchesPerYear");
const matchesWonPerTeam = require("./matchesWonPerTeam");
const extraRunsPerTeam = require("./extraRunsPerTeam");
const topEconomicalBowlers = require("./topEconomicalBowlers");
const winTossWinMatch = require("./winTossWinMatch");
const mostManOfMatch = require("./mostManOfMatch");
const battingStrikeRate = require("./battingStrikeRate");
const mostDismissals = require("./mostDismissals");
const superOverEconomy = require("./superOverEconomy");

const matchesCsvPath = "src/data/matches.csv";
const deliveriesCsvPath = "src/data/deliveries.csv";
const outputDirectory = "src/public/output";

const saveOutput = (filePath, objName, displayName) => {
  fs.writeFile(filePath, JSON.stringify(objName), (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`\n ${displayName} is saved in ${filePath}`);
    }
  });
};

fs.access(outputDirectory, (error) => {
  if (error) {
    fs.mkdir(outputDirectory, (error) => {
      if (error) {
        console.error(error);
      } else {
        console.log("output directory created successfully");
      }
    });
  }
});

csv()
  .fromFile(matchesCsvPath)
  .then((matchesData) => {
    const matchesPerYearResult = matchesPerYear(matchesData);
    const matchesPerYearOutputPath = "src/public/output/matchesPerYear.json";

    saveOutput(
      matchesPerYearOutputPath,
      matchesPerYearResult,
      "matchesPerYear"
    );

    const matchesWonPerTeamResult = matchesWonPerTeam(matchesData);
    const matchesWonPerTeamOutputPath =
      "src/public/output/matchesWonPerTeam.json";

    saveOutput(
      matchesWonPerTeamOutputPath,
      matchesWonPerTeamResult,
      "matchesWonPerTeam"
    );

    const winTossWinMatchResult = winTossWinMatch(matchesData);
    const winTossWinMatchOutputPath = "src/public/output/winTossWinMatch.json";

    saveOutput(
      winTossWinMatchOutputPath,
      winTossWinMatchResult,
      "winTossWinMatch"
    );

    const mostManOfMatchResult = mostManOfMatch(matchesData);
    const mostManOfMatchOutputPath = "src/public/output/mostManOfMatch.json";

    saveOutput(
      mostManOfMatchOutputPath,
      mostManOfMatchResult,
      "mostManOfMatch"
    );

    csv()
      .fromFile(deliveriesCsvPath)
      .then((deliveriesData) => {
        const extraRunsPerTeamResult = extraRunsPerTeam(
          deliveriesData,
          matchesData,
          2016
        );
        const extraRunsPerTeamOutputPath =
          "src/public/output/extraRunsPerTeam.json";

        saveOutput(
          extraRunsPerTeamOutputPath,
          extraRunsPerTeamResult,
          "extraRunsPerTeam"
        );

        const topEconomicalBowlersResult = topEconomicalBowlers(
          deliveriesData,
          matchesData,
          2015
        );
        const topEconomicalBowlersOutputPath =
          "src/public/output/topEconomicalBowlers.json";

        saveOutput(
          topEconomicalBowlersOutputPath,
          topEconomicalBowlersResult,
          "topEconomicalBowlers"
        );

        const battingStrikeRateResult = battingStrikeRate(
          deliveriesData,
          matchesData,
          "MS Dhoni"
        );
        const battingStrikeRateOutPutPath =
          "src/public/output/battingStrikeRate.json";

        saveOutput(
          battingStrikeRateOutPutPath,
          battingStrikeRateResult,
          "battingStrikeRate"
        );

        const mostDismissalsResult = mostDismissals(deliveriesData, "MS Dhoni");
        const mostDismissalsOutputPath =
          "src/public/output/mostDismissals.json";

        saveOutput(
          mostDismissalsOutputPath,
          mostDismissalsResult,
          "mostDismissals"
        );

        const superOverEconomyResult = superOverEconomy(deliveriesData);
        const superOverEconomyOutputPath =
          "src/public/output/superOverEconomy.json";

        saveOutput(
          superOverEconomyOutputPath,
          superOverEconomyResult,
          "superOverEconomy"
        );
      });
  });
