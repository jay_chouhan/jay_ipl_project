const matchesPerYear = matchesData => {
  return matchesData.reduce((accumulator, match) => {
    if (accumulator[match.season] === undefined) {
      accumulator[match.season] = 0;
    }
    accumulator[match.season] += 1;

    return accumulator;
  }, {});
};

module.exports = matchesPerYear;
