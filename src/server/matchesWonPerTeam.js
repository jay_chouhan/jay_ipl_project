const matchesWonPerTeam = matchesData => {
  return matchesData.reduce((accumulator, match) => {
    if (accumulator[match.season] === undefined) {
      accumulator[match.season] = {};
    }
    if (accumulator[match.season][match.winner] === undefined) {
      accumulator[match.season][match.winner] = 0;
    }
    accumulator[match.season][match.winner] += 1;
    return accumulator;
  }, {});
};

module.exports = matchesWonPerTeam;
