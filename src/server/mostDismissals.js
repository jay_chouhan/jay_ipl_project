const mostDismissals = (deliveriesData, playerName) => {
  const dismissals = deliveriesData
    .filter(delivery => delivery.player_dismissed === playerName)
    .filter(delivery => delivery.dismissal_kind !== "run out")
    .reduce((accumulator, delivery) => {
      if (accumulator[delivery.bowler] === undefined) {
        accumulator[delivery.bowler] = 0;
      }
      accumulator[delivery.bowler] += 1;

      return accumulator;
    }, {});

  const topBowler = Object.entries(dismissals)
    .sort((a, b) => a[1] - b[1])
    .pop();

  return {
    player: playerName,
    Bowler: topBowler[0],
    times: topBowler[1]
  };
};

module.exports = mostDismissals;
