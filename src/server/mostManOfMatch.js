const mostManOfMatch = matchesData => {
  let manOfMatch = matchesData.reduce((accumulator, match) => {
    if (accumulator[match.season] === undefined) {
      accumulator[match.season] = {};
    }
    if (accumulator[match.season][match.player_of_match] === undefined) {
      accumulator[match.season][match.player_of_match] = 0;
    }
    accumulator[match.season][match.player_of_match] += 1;

    return accumulator;
  }, {});

  Object.keys(manOfMatch).forEach(season => {
    const player = Object.entries(manOfMatch[season])
      .sort((a, b) => a[1] - b[1])
      .pop();
    manOfMatch[season] = {
      [player[0]]: player[1]
    };
  });
  return manOfMatch;
};

module.exports = mostManOfMatch;
