const superOverEconomy = deliveriesData => {
  const superBowlers = deliveriesData
    .filter(delivery => delivery.is_super_over === "1")
    .reduce((accumulator, delivery) => {
      if (accumulator[delivery.bowler] === undefined) {
        accumulator[delivery.bowler] = {
          runs: 0,
          balls: 0
        };
      }
      accumulator[delivery.bowler]["runs"] +=
        parseInt(delivery.wide_runs) +
        parseInt(delivery.noball_runs) +
        parseInt(delivery.batsman_runs);

      accumulator[delivery.bowler]["balls"] += 1;

      accumulator[delivery.bowler]["economy"] = (
        (accumulator[delivery.bowler]["runs"] /
          accumulator[delivery.bowler]["balls"]) *
        6
      ).toFixed(2);

      return accumulator;
    }, {});

  return Object.entries(superBowlers)
    .sort((a, b) => a[1].economy - b[1].economy)
    .shift();
};

module.exports = superOverEconomy;
