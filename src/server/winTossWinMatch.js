const winTossWinMatch = matchesData => {
  return matchesData
    .filter(match => match.toss_winner === match.winner)
    .reduce((accumulator, match) => {
      if (accumulator[match.winner] === undefined) {
        accumulator[match.winner] = 0;
      }
      accumulator[match.winner] += 1;

      return accumulator;
    }, {});
};

module.exports = winTossWinMatch;
